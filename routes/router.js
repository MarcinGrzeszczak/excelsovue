const Router = require('express-promise-router')
const product = require('./endpoints/product')
const detail = require('./endpoints/detail')

const router = new Router()

router.use('/product', product)
router.use('/detail', detail)

module.exports = router