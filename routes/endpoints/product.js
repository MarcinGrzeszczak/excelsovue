const Router = require('express-promise-router')
const {query} = require('../../db')


const avaibleFields = ['name', 'short_desc', 'stock', 'picture', 'price']

const router = new Router()

router.get('/all', async(req, res) => {
    const {rows} = await query('SELECT * FROM products')
    res.send(rows)
})

router.get('/all/id', async(req, res) => {
    const {rows} = await query('SELECT id FROM products',[])
    res.send(rows.map(e => e.id))
})

router.get('/:id', async(req, res) => {
    const {id} = req.params
    const {rows} = await query('SELECT * FROM products WHERE id = $1 ',[id])
    res.send(rows)
})

router.get('/:id/:field', async(req, res) => {

    const {id,field} = req.params
    if(avaibleFields.includes(field)){
        const {rows} = await query(`SELECT ${field} FROM products WHERE id = $1 `,[id])
        return res.send(rows[0])
    }
    return res.send([])
})

module.exports = router