const Router = require('express-promise-router')
const {query} = require('../../db')

const router = new Router()

const avaibleFields = ['description', 'variety', 'score', 'origin', 'process']


router.get('/:id/:field', async(req, res) => {

    const {id} = req.params
    let {field} = req.params

    if(avaibleFields.includes(field)){
        if(field === 'process') {
            const {rows} = await query('SELECT processes.description FROM product_detail  INNER JOIN processes ON product_detail.process_id=processes.id WHERE product_detail.product_id = $1',[id])
            return res.send({[field] : rows[0].description})
        }

        const {rows} = await query(`SELECT ${field} FROM product_detail WHERE product_id = $1 `,[id])
        return res.send(rows[0])
    }
    return res.send([])
})


module.exports = router