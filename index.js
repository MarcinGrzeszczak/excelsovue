const express = require('express')
const cors = require('cors')
const db = require('./db')
const routes = require('./routes/router')
const app = express()


const staticFrontend = express.static('dist')


try{
    db.dbConnect()
    
    app.use(cors())
    app.use('/api', (req,res,next) => {
        console.log(`host: ${req.get('host')}`)
        if(req.get('host').includes('localhost'))
            next()
        else
            res.send({status:404, msg:"AccessDenied"})
    })

    
    app.use('/api',routes)

    app.use(staticFrontend)
    app.listen(3000, () => {
        console.log('Server started at port 3000')
    })
}
catch (e) {
    console.log('Server Error: '+ e)
}