import StartPage from './pages/StartPage'
import Details from './pages/Details'
import CartPage from './pages/Cart'

export const internalRoutes = [
    {name:'home', path: '/', component: StartPage},
    {name:'details', path: '/product/:id', component: Details, props:true},
    {name:'cart', path: '/cart', component: CartPage}
]

export const externalRoutes = [
    {
      path: '/instagram',
      name:'instagram',
      beforeEnter() { 
        window.location='https://www.instagram.com'
      }
    },
    {
      path: '/facebook',
      name:'facebook',
      beforeEnter() {
        window.location = 'https://www.facebook.com'
      }
    },
    {
      path: '/twitter',
      name:'twitter',
      beforeEnter() {
        window.location = 'https://www.twitter.com'
      }
    },
]