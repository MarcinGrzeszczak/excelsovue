
const KEY_NAME = 'cart'

function addItem(productId, quantity) {
    const tmpCartItems = getAllItems()
    const newItem = {
        quantity
    }

    if(Object.keys(tmpCartItems).find(id => productId == id)) {
        newItem.quantity += tmpCartItems[productId].quantity
    }
    tmpCartItems[productId] = newItem
    setCartStorage(tmpCartItems)
}

function deleteItem(productId) {
    const tmpCartItems = getAllItems()
    delete tmpCartItems[productId]
    setCartStorage(tmpCartItems)
}

function getAllItems() {
    return JSON.parse(window.localStorage.getItem(KEY_NAME)) || {}
}

function setCartStorage(data) {
    window.localStorage.setItem(KEY_NAME, JSON.stringify(data))
}

export default {addItem, deleteItem, getAllItems, setCartStorage}