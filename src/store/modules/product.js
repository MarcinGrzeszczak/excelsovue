import {PRODUCT_FIELDS} from '../../ApiRoutes'
import {DETAIL_FIELDS} from '../../ApiRoutes'
const state = {
    products: {}
}

const getters = {
    getName(state) {
        return function(id) {
            return getValueformProduct(state,id,PRODUCT_FIELDS.NAME)
        }
    },
    getPicture(state) {
        return function(id) {
            return getValueformProduct(state,id,PRODUCT_FIELDS.PICTURE)
        }
    },
    getPrice(state) {
        return  function(id) {
            return getValueformProduct(state,id,PRODUCT_FIELDS.PRICE)
        }
    },
    getShortDesc(state) {
        return function(id) {
            return getValueformProduct(state,id,PRODUCT_FIELDS.DESC)
        }
    },
    getStock(state) {
        return function(id) {
            return getValueformProduct(state,id,PRODUCT_FIELDS.STOCK)
        }
    },
    getDescription(state) {
        return function(id) {
            return getValueformProduct(state, id, DETAIL_FIELDS.DESC)
        }
    },
    getOrigin(state) {
        return function(id) {
            return getValueformProduct(state, id, DETAIL_FIELDS.ORIGIN)
        }
    },
    getVariety(state) {
        return function(id) {
            return getValueformProduct(state, id, DETAIL_FIELDS.VARIETY)
        }
    },
    getScore(state) {
        return function(id) {
            return getValueformProduct(state, id, DETAIL_FIELDS.SCORE)
        }
    },
    getProcess(state) {
        return function(id) {
            return getValueformProduct(state, id, DETAIL_FIELDS.PROCESS)
        }
    },
}


const mutations = {
    setName(state,payload) {
        setValueToProduct(state,PRODUCT_FIELDS.NAME, payload)
    },
    setPicture(state, payload){
        setValueToProduct(state,PRODUCT_FIELDS.PICTURE, payload)
    },
    setPrice(state, payload){
        setValueToProduct(state,PRODUCT_FIELDS.PRICE, payload)
    },
    setShortDesc(state, payload){
        setValueToProduct(state,PRODUCT_FIELDS.DESC, payload)
    },
    setStock(state, payload){
        setValueToProduct(state,PRODUCT_FIELDS.STOCK, payload)
    },
    setDescription(state, payload) {
        setValueToProduct(state,DETAIL_FIELDS.DESC, payload)
    },
    setOrigin(state, payload) {
        setValueToProduct(state,DETAIL_FIELDS.ORIGIN, payload)
    },
    setVariety(state, payload) {
        setValueToProduct(state, DETAIL_FIELDS.VARIETY, payload)
    },
    setScore(state, payload) {
        setValueToProduct(state, DETAIL_FIELDS.SCORE, payload)
    },
    setProcess(state, payload) {
        setValueToProduct(state, DETAIL_FIELDS.PROCESS, payload)
    },
}

const actions = {
    setName(context,payload) {
        context.commit('setName', payload)
    },
    setPicture(context, payload){
        context.commit('setPicture', payload)
    },
    setPrice(context, payload){
        context.commit('setPrice', payload)
    },
    setShortDesc(context, payload){
        context.commit('setShortDesc', payload)
    },
    setStock(context, payload){
        context.commit('setStock', payload)
    },
    setDescription(context, payload) {
        context.commit('setDescription',payload)
    },
    setOrigin(context, payload) {
        context.commit('setOrigin',payload)
    },
    setVariety(context, payload) {
        context.commit('setVariety', payload)
    },
    setScore(context, payload) {
        context.commit('setScore', payload)
    },
    setProcess(context, payload) {
        context.commit('setProcess', payload)
    },
}

function getValueformProduct(state,id,field){
    if(!state.products[id])
        return null
    return state.products[id][field]
}

function setValueToProduct(state,field,payload) {
    if(!state.products[payload.id])
        state.products[payload.id] = {}
    state.products[payload.id][field] = payload.value
}

export default {state, getters, mutations, actions}