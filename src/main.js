import Vue from 'vue'
import VueRouter from 'vue-router'

import {internalRoutes, externalRoutes} from './routes'
import {store} from './store/store'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [...internalRoutes, ...externalRoutes]
})

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
