export default `
sources:
    omv:
        type: OMV
        max_zoom: 12
        min_display_zoom: 1
# global description of the map, in this example
# the map background color is white
scene:
    background:
        color: rgba(240, 240, 240,1)

# section contains the style information for the layers
# that are present on the map
layers:
    # user defined name of the rendering layer
    water_areas:
        # the section defines where the rendering layer takes
        # its data from source: omv is mandatory for the Vector Tile API
        # layer: water specifies what vector layer is taken
        # for the rendering see REST API documentation for the
        # list of available layers.
        data: {source: omv, layer: water}
        # section defines how to render the layer
        draw:
            polygons:
                order: 1 # z-order of the layer
                color: rgba(221, 221, 255,1)
    building:
        data: {source: omv, layer: buildings}
        draw:
            text:
                collide: true
                font:
                    fill: rgba(130, 130, 130, 1.0)
                    size: 10px
            draw:
                polygons:
                    order: 1 # z-order of the layer
                    color: rgba(0,0,0,0)
    landuse:
        data: {source: omv, layer: landuse}
        draw:
            text:
                collide: true
                font:
                    fill: rgba(130, 130, 130, 1.0)
                    size: 10px
                draw:
                    polygons:
                        order: 1 # z-order of the layer
                        color: rgba(0,0,0,0)
    road:
        data: {source: omv, layer: roads}
        draw:
            lines:
                order: 2
                color: rgba(223, 223, 223,1)
                # the width is set in the world meters
                width: 4
        major_road:
            # the filter section narrows down to what features of the
            # data layer the style must be applied to
            filter:
                kind: 'major_road'
                kind_detail: 'secondary'
            draw:
                text:
                    collide: true
                    font:
                        fill: rgba(130, 130, 130, 1.0)
                        size: 15px
                lines:
                    color: rgba(206, 206, 206, 1.0)
                    # the width is set in the screen pixels
                    width: 5px
        major_road_tertiary:
            filter:
                kind: 'major_road'
                kind_detail: 'tertiary'
            draw:
                text:
                    collide: true
                    font:
                        fill: rgba(130, 130, 130, 1.0)
                        size: 12px
                lines:
                    color: rgba(223, 223, 223,1)
                    width: 3px

`