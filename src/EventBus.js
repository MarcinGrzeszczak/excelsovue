import Vue from 'vue'

export default new Vue({
    methods: {
        emitSearchProduct(name) {
            this.$emit('searchedProduct',name)
        },
        listenSearchProduct(cb) {
            this.$on('searchedProduct',cb)
        },
        emitAddToCart(quantity) {
            this.$emit('AddToCartBus',quantity)
        },
        listenAddToCart(cb){
            this.$on('AddToCartBus',cb)
        }
    }
})