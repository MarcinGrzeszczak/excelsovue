import axios from 'axios'

const root = 'http://localhost:3000/api'

axios.defaults.baseURL = root

console.log('inited Api')
console.log(axios)
export const PRODUCT_FIELDS = {
    NAME:'name', 
    DESC:'short_desc', 
    STOCK:'stock', 
    PICTURE:'picture', 
    PRICE:'price'}

export const DETAIL_FIELDS = {
    DESC:'description', 
    VARIETY: 'variety', 
    SCORE: 'score', 
    ORIGIN: 'origin', 
    PROCESS: 'process'}


export const product = {
    getAllProducts: () => axios.get('/product/all'),
    getAllProductsID: () => axios.get(`/product/all/id`),
    getProduct: (id, field='') =>axios.get(`/product/${id}/${field}`)
}

export const detail = {
    getDetail: (id, field='') => axios.get(`/detail/${id}/${field}`)
}
