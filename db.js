const {Pool} = require('pg')
let pool = {}
function dbConnect() {
    pool = new Pool({
        connectionString: process.env.DATABASE_URL,
        ssl: {
            rejectUnauthorized: false
        }
        
    })
}

function query(text, params) {
    if(!pool)
        return ''    
    return pool.query(text, params)

}
module.exports = {
    dbConnect,
    query
}